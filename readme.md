# Deprecation
This project is deprecated... Use [Docker/jekyll-lcsb](https://git-r3lab.uni.lu/R3/docker/jekyll-lcsb) instead!


# Information
This is a base image for Jekyll-based web-pages.
It contains some of the ruby gems required to build Jekyll websites preinstalled.
