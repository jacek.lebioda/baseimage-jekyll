FROM ruby:latest
MAINTAINER Jacek Lebioda "jacek.lebioda@uni.lu"
COPY Gemfile .
RUN gem install bundler:2.0.2 && \
    gem install sassc -- --disable-march-tune-native && \
    bundle install && bundle update
